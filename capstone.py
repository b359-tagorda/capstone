from abc import ABC, abstractmethod

class Person(ABC):
    def __init__(self, first_name, last_name, email, department):
        self.__first_name = first_name
        self.__last_name = last_name
        self.__email = email
        self.__department = department

    @property
    def firstName(self):
        return self.__first_name

    @firstName.setter
    def firstName(self, value):
        self.__first_name = value

    @property
    def lastName(self):
        return self.__last_name

    @lastName.setter
    def lastName(self, value):
        self.__last_name = value

    @property
    def email(self):
        return self.__email

    @email.setter
    def email(self, value):
        self.__email = value

    @property
    def department(self):
        return self.__department

    @department.setter
    def department(self, value):
        self.__department = value

    @abstractmethod
    def checkRequest(self, request):
        pass

    @abstractmethod
    def addRequest(self, request):
        pass

    def getFullName(self):
        return f"{self.__first_name} {self.__last_name}"

    def login(self):
        return f"{self.__email} has logged in"

    def logout(self):
        return f"{self.__email} has logged out"

class Employee(Person):
    def __init__(self, first_name, last_name, email, department):
        super().__init__(first_name, last_name, email, department)

    def checkRequest(self, request):
        pass

    def addRequest(self, request=None):
        print("Request has been added")
        return "Request has been added"

class Admin(Person):
    def __init__(self, first_name, last_name, email, department):
        super().__init__(first_name, last_name, email, department)

    def checkRequest(self, request):
        pass

    def addRequest(self, request=None):
        print("Request has been added")
        return "Request has been added"

    def addUser(self):
        print("User has been added")
        return "User has been added"

class TeamLead(Person):
    def __init__(self, first_name, last_name, email, department):
        super().__init__(first_name, last_name, email, department)
        self.members = []

    def checkRequest(self, request):
        pass

    def addRequest(self, request=None):
        print("Request has been added")
        return "Request has been added"

    def addMember(self, employee):
        if isinstance(employee, Employee):
            self.members.append(employee)
            return "Member has been added"

    def get_members(self):
        return self.members

class Request:
    def __init__(self, name, requester, dateRequested, status="Open"):
        self.name = name
        self.requester = requester
        self.dateRequested = dateRequested
        self.status = status

    def updateRequest(self):
        return f"Request {self.name} has been updated"

    def closeRequest(self):
        return f"Request {self.name} has been closed"

    def cancelRequest(self):
        return f"Request {self.name} has been cancelled"

    def set_status(self, status):
        self.status = status

# Test cases
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"


teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.get_members():
 print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())
